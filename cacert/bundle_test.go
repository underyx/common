package cacert

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"
)

func TestBundleWrite_EmptyContent(t *testing.T) {
	// prepare directory for import path
	tmpDir, err := ioutil.TempDir("", "")
	if err != nil {
		t.Fatalf("unable to create test dir tree: %v\n", err)
	}
	defer os.RemoveAll(tmpDir)

	// import bundle
	path := filepath.Join(tmpDir, "ca-bundle.crt")
	content := ""

	err = bundle{content: content, path: path}.write()
	if err != nil {
		t.Fatal(err)
	}

	// check that import path is not a file
	_, err = os.Stat(path)
	if !os.IsNotExist(err) {
		if err == nil {
			t.Errorf("Expected %s to NOT exist when additional ca cert bundle is an empty string", path)
		} else {
			t.Fatalf("Unexpected error: %v", err)
		}
	}
}

func TestBundleWrite_EmptyPath(t *testing.T) {
	err := bundle{content: "xyz", path: ""}.write()
	if err != nil {
		t.Fatal(nil)
	}
}

func TestBundleWrite_ExistingImportDir(t *testing.T) {
	// prepare directory for import path
	tmpDir, err := ioutil.TempDir("", "")
	if err != nil {
		t.Fatalf("unable to create test dir tree: %v\n", err)
	}
	defer os.RemoveAll(tmpDir)

	gitconfigPath = filepath.Join(tmpDir, "gitconfig")

	// import bundle
	path := filepath.Join(tmpDir, "ca-bundle.crt")
	content := "Some Bundle!"

	err = bundle{content: content, path: path}.write()
	if err != nil {
		t.Fatal(err)
	}

	expectedContent := fmt.Sprintf("%s\n", content)
	// check imported bundle
	compareFile(t, path, expectedContent)
}

func TestBundleWrite_MissingImportDir(t *testing.T) {
	// prepare directory for import path
	tmpDir, err := ioutil.TempDir("", "")
	if err != nil {
		t.Fatalf("unable to create test dir tree: %v\n", err)
	}
	defer os.RemoveAll(tmpDir)

	gitconfigPath = filepath.Join(tmpDir, "gitconfig")

	// import bundle
	path := filepath.Join(tmpDir, "does", "not", "exist", "ca-bundle.crt")
	content := "Some Bundle!"

	err = bundle{content: content, path: path}.write()
	if err != nil {
		t.Fatal(err)
	}

	expectedContent := fmt.Sprintf("%s\n", content)
	// check imported bundle
	compareFile(t, path, expectedContent)
}

func TestBundleWrite_ExistingFile(t *testing.T) {
	// prepare directory for import path
	tmpDir, err := ioutil.TempDir("", "")
	if err != nil {
		t.Fatalf("unable to create test dir tree: %v\n", err)
	}
	defer os.RemoveAll(tmpDir)

	gitconfigPath = filepath.Join(tmpDir, "gitconfig")

	// create file prior to the import
	path := filepath.Join(tmpDir, "ca-bundle.crt")
	originalValue := "some original value\n"
	err = ioutil.WriteFile(path, []byte(originalValue), 0644)
	if err != nil {
		t.Fatal(err)
	}

	// import bundle
	content := "Some Bundle!"
	err = bundle{content: content, path: path}.write()
	if err != nil {
		t.Fatal(err)
	}

	expectedContent := fmt.Sprintf("%s%s\n", originalValue, content)
	// check imported bundle
	compareFile(t, path, expectedContent)
}

func TestBundleWrite_Twice(t *testing.T) {
	// prepare directory for import path
	tmpDir, err := ioutil.TempDir("", "")
	if err != nil {
		t.Fatalf("unable to create test dir tree: %v\n", err)
	}
	defer os.RemoveAll(tmpDir)

	gitconfigPath = filepath.Join(tmpDir, "gitconfig")

	// import bundle
	path := filepath.Join(tmpDir, "ca-bundle.crt")
	content := "Some Bundle!"

	err = bundle{content: content, path: path}.write()
	if err != nil {
		t.Fatal(err)
	}

	// write a duplicate entry
	err = bundle{content: content, path: path}.write()
	if err != nil {
		t.Fatal(err)
	}

	expectedContent := fmt.Sprintf("%s\n%s\n", content, content)
	// check imported bundle
	compareFile(t, path, expectedContent)
}

func TestBundleWrite_Gitconfig(t *testing.T) {
	// prepare directory for import path
	tmpDir, err := ioutil.TempDir("", "")
	if err != nil {
		t.Fatalf("unable to create test dir tree: %v\n", err)
	}
	defer os.RemoveAll(tmpDir)

	gitconfigPath = filepath.Join(tmpDir, "gitconfig")

	// import bundle
	path := filepath.Join(tmpDir, "does", "not", "exist", "ca-bundle.crt")
	content := "Some Bundle!"

	err = bundle{content: content, path: path}.write()
	if err != nil {
		t.Fatal(err)
	}

	want := fmt.Sprintf("[http] sslCAInfo = %s\n", path)

	// check gitconfig file
	compareFile(t, gitconfigPath, want)
}

func compareFile(t *testing.T, path string, content string) {
	got, err := ioutil.ReadFile(path)
	if err != nil {
		t.Fatal(err)
	}

	if content != string(got) {
		t.Errorf("Expected match to be\n%#v\nbut got\n%#v", content, string(got))
	}
}
