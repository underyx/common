package pathfilter

import "strings"

// Filter is a path filter.
type Filter struct {
	ExcludedPaths []string // ExcludedPaths is a list of excluded patterns.
}

// IsExcluded tells whether a given path is excluded.
func (f Filter) IsExcluded(path string) bool {
	for _, p := range f.ExcludedPaths {
		if matched, _ := Match(p, path); matched {
			return true
		}
	}
	return false
}

// String exports the filter as comma-separated list of paths.
func (f Filter) String() string {
	return strings.Join(f.ExcludedPaths, ",")
}
